package exemples;

public class Programme01_Def_et_AccesDirect {

    public static void main(String[] args) {
    
       
        // Déclaration et initialisation d'un tableau à 2 dimensions:  3 lignes 5 colonnes
        
        int[][] tableau= {
       
           //    indices colonnes 
           // 0    1    2    3   4
                                      // indices lignes
           {  15, -10,  20, 30, 40 }, //       0    
           {  33,  42, -31, 14, 17 }, //       1
           { -26,  24, -10, 12, 15 }  //       2
       
       };
       
       // Affichage de la valeur située dans la ligne d'indice 1 et la colonne d'indice 2

       System.out.println();
       System.out.print("La valeur stockée dans la ligne d'indice 1 et la colonne d'indice 2 est:     ");
       
       System.out.println(tableau[1][2]);
       
       System.out.println();
        
    }
}
