package exemples;

public class Programme02_NbLignes {

    public static void main(String[] args) {
    
       int[][] tableau= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       
       System.out.print("\nNombre de lignes du tableau:  ");
       
       // Nombre de lignes du tableau:     tableau.length
       
       System.out.println(tableau.length);
       
       System.out.println();
       
      
    }
}
