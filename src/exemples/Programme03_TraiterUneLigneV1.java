package exemples;

public class Programme03_TraiterUneLigneV1 {

    public static void main(String[] args) {
    
       
        
       int[][] tableau= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       int  nbCol = 5;
      
        
       // Affichage  des éléments de la ligne d'indice 1
       
     
       System.out.print("\nLes éléments de la ligne d'indice 1 sont:    ");
       
       for( int col=0; col < nbCol; col++ ){ // On fait varier l'indice colonne de 0 à 4
       
          System.out.print(tableau[1][col]+"  ");   // On reste dans la ligne 1 mais col varie de 0 à 4
       }
        
       System.out.println("\n"); 
       
 
    }
}
