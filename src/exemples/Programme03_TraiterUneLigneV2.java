package exemples;

public class Programme03_TraiterUneLigneV2 {

    public static void main(String[] args) {
    
          
       int[][] tableau= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
     
       // Affichage  des éléments de la ligne d'indice 1
       
     
       System.out.print("\nLes éléments de la ligne d'indice 1 sont:    ");
       
       for( int x : tableau[1]){ // l'entier x parcourt la ligne d'indice 1
       
            System.out.print( x +"  ");   // On affiche x
       }
        
       System.out.println("\n"); 
       
    }
}
