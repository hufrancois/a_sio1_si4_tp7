package exemples;

public class Programme04_AfficheToutV1 {

    public static void main(String[] args) {
    
       int[][] tableau= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // Affichage de tous les éléments du tableau
               
        int nbLig  = tableau.length;  // 3 dans notre cas
        int nbCol  = 5;
        
        System.out.println("\nAffichage de tous les éléments du tableau\n");
       
        for(int lig=0;lig < nbLig; lig++ ) {
        
           for(int col=0;col < nbCol;col++){
           
               //System.out.printf("%3d ",tableau[lig][col]);   
              
               System.out.print(tableau[lig][col]+ " ");
           }
        
           System.out.println();
        }
        
        System.out.println();
    }
}
