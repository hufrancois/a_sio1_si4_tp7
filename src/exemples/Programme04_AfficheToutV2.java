package exemples;

public class Programme04_AfficheToutV2 {

 public static void main(String[] args) {
    
    int[][] tableau= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
    };
               
    System.out.println("\nAffichage de tous les éléments du tableau boucles for each\n");
       
    for ( int[] ligne : tableau ) { // pour chaque ligne (de type tableau d'entiers int[] )du tableau
        
       for( int elt : ligne ){ // pour chaque element elt du tableau ligne
           
                System.out.printf("%3d ",elt);  // affciher elt
       }
        
       System.out.println();
     }
       
     System.out.println();
    }
}
