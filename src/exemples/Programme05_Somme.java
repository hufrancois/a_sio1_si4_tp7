package exemples;

public class Programme05_Somme {

    public static void main(String[] args) {
    
       int[][] tableau= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
       };
       
       // Affichage de tous les éléments du tableau
               
       int nbLig  = tableau.length;  
       int nbCol  = 5;
        
       int somme=0;
        
       for(int lig=0;lig < nbLig; lig++ ) {
        
          for(int col=0;col < nbCol;col++){
           
               somme+=tableau[lig][col];               
          }
       }
        
       System.out.println("\nLa somme de tous les éléments du tableau vaut:  " + somme);
       System.out.println();
    }
}
