package exemples;

public class Programme06_TraiterUneColonne {

   public static void main(String[] args) {
    
      int[][] tableau= {
       
           {  15, -10,  20, 30, 40 },
           {  33,  42, -31, 14, 17 },
           { -26,  24, -10, 12, 15 }
       
      };
       
      int nbLignes=tableau.length; // c'est à dire 3
       
      // Affichage des éléments de la colonne d'indice 2
    
      System.out.println("\nAffichage des éléments de la colonne d'indice 2\n");
       
      for(int lig=0; lig< nbLignes; lig++ ){ // pour lig variant de 0 à 2  
       
         System.out.printf("%3d\n",tableau[lig][2]); // l'indice de colonne reste fixé   à 2
                                                     // l'indice de ligne lig varie de 0 à 4  
      }   
      System.out.println();
    }
}
