package exemples;

public class Programme07_TableauIrregulier {

    public static void main(String[] args) {
    
       int[][] tableau= {
       
           {  15, -10,  20, 30,  40 },                    //  5 colonnes
           {  33,  42 },                                  //  2 colonnes
           { -26,  24, -10, 12,  15, 67, 12, 8 },         //  8 colonnes  
           {  55 },                                       //  1 colonne
           {  33,  85,  34},                              //  3 colonnes
           {  77,  45,   4, 57, -11, 22 },                //  6 colonnes  
           {     },                                       //  0 colonne
           {  38,  41,  94, 27,  42, 66, 72, 2,  4,  37}, // 10 colonnes
           {  12,  15,  17 }                              //  3 colonnes
       };
       
       // Affichage de tous les éléments du tableau
               
       // int nbLig  = tableau.length;  
       
        System.out.println("\nAffichage de tous les éléments du tableau\n");
       
        for(int lig=0;lig < tableau.length; lig++ ) {
        
           for(int col=0;col < tableau[lig].length; col++){
           
                System.out.printf("%3d ",tableau[lig][col]);   
               
           }
        
           System.out.println();
        }
        
        System.out.println("\n");
    }
}
