
package exemples;

public class Programme08_TableauPersonnes {

    public static void main(String[] args) {
       
       // Dans le tableau ci-dessous chaque ligne représente une équipe
       // Les équipes n'ont pas toutes la même taille, l'une d'entr'elles est vide 
        
        String[][] tableauPersonnes = {
           
           {"Jean-Marc","Blandine"},                                // indice ligne 0: 2 colonnes
           {"Pierre",   "Sophie",  "Elise" },                       // indice ligne 1: 3 colonnes
           {"Patrick",  "Bruno" ,  "Martine","Joséphine","Sarah"},  // indice ligne 2: 5 colonnes
           {"Laurent",  "Fabienne","Alain"},                        // indice ligne 3: 3 colonnes
           {},                                                      // indice ligne 4: 0 colonne
           {"Valentine","Jacques"}                                  // indice ligne 5: 2 colonnes
       
       }; 
       
       // Affichage des effectifs de chaque équipe
        
       System.out.println("\nEffectifs des équipes\n"); 
       
       for( int eq=0 ; eq < tableauPersonnes.length ;  eq++){
       
           System.out.println("Equipe: "+ eq + " Effectif: " + tableauPersonnes[eq].length );
       }
       
       System.out.println();
        
       // Affichage de la composition de l'équipe  d'indice 2
        
       System.out.print("\nComposition de l'équipe d'indice 2: "); 
       
       for(int i=0;i< tableauPersonnes[2].length;i++){
       
           System.out.print(tableauPersonnes[2][i]+" ");
       }
       
        System.out.println("\n");
    }
}



