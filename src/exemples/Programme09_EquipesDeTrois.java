
package exemples;

public class Programme09_EquipesDeTrois {

    public static void main(String[] args) {
       
       // Une ligne par équipe
       // les équipes n'ont pas toutes la même taille, l'une d'entr'elles est vide 
        
       String[][] tableauPersonnes = {
           
           {"Jean-Marc","Blandine"},                                // 2 colonnes
           {"Pierre",   "Sophie",  "Elise" },                       // 3 colonnes
           {"Patrick",  "Bruno" ,  "Martine","Joséphine","Sarah"},  // 5 colonnes
           {"Laurent",  "Fabienne","Alain"},                        // 3 colonnes
           {},                                                      // 0 colonne
           {"Valentine","Jacques"}                                  // 2 colonne
       
       }; 

       // Afficher la composition des équipes dont l'effectif est 3

       
       System.out.println("Composition des équipes de 3 personnes\n");
       
       for( int nEq=0 ; nEq < tableauPersonnes.length; nEq++){
       
           if( tableauPersonnes[nEq].length ==3 ){
           
              System.out.printf("Equipe %d:  ", nEq);
               
              for( int np =0 ; np < tableauPersonnes[nEq].length ; np++){
            
                   System.out.print(tableauPersonnes[nEq][np]+" ");
              }
              System.out.println();
           }
       }
       System.out.println("\n");
    }
}



